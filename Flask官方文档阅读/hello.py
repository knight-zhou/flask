#!/usr/bin/env python
#coding:utf-8

from flask import Flask,request,url_for

#创建一个类的实例
app = Flask(__name__)

@app.route('/')
def  hello_world():
    return "Index Page!"

@app.route('/hello')
def hello():
    return "hello World..."


#通过把 URL 的一部分标记为 <variable_name> 就可以在 URL 中添加变量。
# 标记的 部分会作为关键字参数传递给函数。
@app.route('/user/<username>')
def aa(username):
    return 'User: %s' %username

#http方法
@app.route('/login',methods=['GET','POST'])
def login():
    if request.method=='POST':
        return "这是post方法"
    else:
        return "这不是post方法"




#防止模块导入时候运行
if __name__=='__main__':
    app.debug = True
    app.run(host='0.0.0.0')



