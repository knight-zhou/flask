web.py官网: http://webpy.org/docs/0.3/tutorial.zh-cn

Flask官网：http://flask.pocoo.org/ 

英文文档 ：http://flask.pocoo.org/docs/ 

中文文档 ：http://dormousehole.readthedocs.org/en/latest/ 
			
		   http://www.pythondoc.com/flask-mega-tutorial/index.html

最新源码 ：https://github.com/mitsuhiko/flask ​

扩展包    ：http://flask.pocoo.org/extensions/ 
 
推荐牛人教程： http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world 

推荐牛人教程之中文翻译版： http://www.pythondoc.com/flask-mega-tutorial/index.html 

推荐观看: https://github.com/humiaozuzu/awesome-flask
 

SQLAlchemy官网：

http://www.sqlalchemy.org/ 

http://docs.sqlalchemy.org/en/latest/orm/tutorial.html

flask视频教程:

http://www.maiziedu.com/course/313/

## 源码网址
[博客-01](https://github.com/linanwx/unrealblue-blog)

主流发布方案: gunicorn+nginx+flask/django