### 数据库模型的三种关系
The monitor collected the exercise books after the class was over！(下课后，班长把练习册收了起来)
```
数据库实体间有三种对应关系：一对一，一对多，多对多。
一对一关系示例：一个学生对应一个学号，是一一对应的
一对多关系示例：一个学生只属于一个班，但是一个班级有多名学生。
多对多关系示例：一个学生可以选择多门课，一门课也有多名学生。
```

## flask中的一对多
```
class Role(db.Model):    
      __tablename__='roles'    
      id = db.Column(db.Integer,primary_key=True)    
      name = db.Column(db.String(64),unique=True)    
      users = db.relationship('User',backref='role')  
      
      def __repr__(self):    
          return '<Role %s>'%self.name    
 class User(db.Model):    
      __tablename__='users'    
      id=db.Column(db.Integer,primary_key=True)    
      name=db.Column(db.String(64),unique=True)    
      role_id = db.Column(db.Integer,db.ForeignKey('roles.id'))  # 设置外键约束，关联 Role表中的id字段
      def __repr__(self):    
          return '<User %s>'%self.name
```
解析： User表中创建了一个名叫role_id的字段,并把它设置成外键,然后把外键关联成roles表中的id字段

需要注意的时这里用的roles.id,roles是我们在Role模型中定义的__tablename__. 这是SQL数据库中一般设置外键的方法.

## flask中的多对多关系
多对多关系，需要借助第三个表来实现。通过第三个表，将多对多关系分解成两个一对多关系。

以我们常见的Student类和Class(课)类为例，一个Student可以选择多个Class(课)，而一门Class也可以被多个Student选择，我们创建一个Relation表来关联Student和Class(课)

```
relation=db.Table('relations',
                db.Column('student_id',db.Integer,db.ForeignKey('students.id')),
                db.Column('class_id',db.Integer,db.ForeignKey('classes.id'))
                )  
  
class Student(db.Model):    # 定义学生表
    __tablename__='students'  
    id=db.Column(db.Integer,primary_key=True)  
    name=db.Column(db.String)  
    classes=db.relationship('Class',secondary=relation,backref=db.backref('students',lazy='dynamic'),lazy='dynamic')  
      
    def __repr__(self):  
        return '<Student %s>'%self.name  
  
class Class(db.Model):     # 定义课程表
    __tablename__='classes'  
    id=db.Column(db.Integer,primary_key=True)  
    name=db.Column(db.String)  
  
    def __repr__(self):  
        return '<CLass %s>'%self.name  
```

解析:  第一行，我们创建了一个Table叫做relation。

之所以在这里创建，是因为我不想创建数据库，而是利用db.Table，将这个表创建到内存中。

relation表中含有两个字段分别作为外键关联到Student类和Class类的主键id中。紧接着创建两个表Student和Class，在Student表中我们创建了一个db.relationship，

用backref在Class类中添加反向引用students。并用secondary指定作为中间表的relation
```
几点说明

1.关联表就是一张简单的表，不是模型，所以注意写法 
2.多对多关系可以在任意一侧定义关系，这里是在student表里定义 
3.secondary参数为关联表名 
4.backref会自动处理另一侧的反向引用，所以你看class表那么简洁

现在查询就变的简单了。 
要查小明选的所有课：小明.classes.all() 
要查所有上语文课的学生:语文.students.all() 
如果小明再选一门数学：小明.classes.add(数学) 
如果小明退选一门化学：小明.classes.remove(化学)
```
SQLAlchemy实现如上，图解如下:
![](./1.png)

![](./2.png)

![](./3.png)