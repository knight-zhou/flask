#!/usr/bin/env python
#coding:utf-8

from flask import Flask,render_template,request,redirect,url_for
import config

app = Flask(__name__)
app.config.from_object(config)

@app.route('/')
def index():
    return u"这是首页"

@app.route('/user/add/')
def add():
    return u"这user是add页"

@app.route('/user/show/')
def show():
    return u'这user是show页面'

@app.route('/admin/index/')
def adminindex():
    return u'这是admin index页面'

@app.route('/admin/add/')
def adminadd():
    return u'这是admin add页面'


if __name__=='__main__':
    app.run()