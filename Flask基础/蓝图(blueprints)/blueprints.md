## 为什么使用蓝图 ？
```
#views.py
from app import app


@app.route('/user/index')
def index():
    return 'user_index'

@app.route('/user/show')
def show():
    return 'user_show'

@app.route('/user/add')
def add():
    return 'user_add'

@app.route('/admin/index')
def adminindex():
    return 'admin_index'

@app.route('/admin/show')
def adminshow():
    return 'admin_show'

@app.route('/admin/add')
def adminadd():
    return 'admin_add'

#从视图方法中，我们看到有6个视图，分别对应admin,user两个不同用户的3个功能index,add,show.
这样写显然没问题，但是明显与python提倡的模块化，优雅的代码特点相违背,即不是很python的python代码.

让我们在这里假想两个问题:
1.如果admin和user不止只有3个功能呢，比如好几百个，导致views的代码量已经达到了上万行？
2.如果我们有多个同事分工开发admin，user和其它可能的模块呢,都同时往一个views里写代码吗，在做版本控制时，提交过程中频繁出现提交冲突了怎么办？
3.加入我们要抛弃admin或者user功能块时怎么办，要一一手动删除所有admin或是user相关的代码吗，这样当然可以，但是会不会太low呢?

当然根据Pythonic特点，我们肯定希望尽可能的把代码尽量的模块化，让我们的代码看起来更加的优雅和顺畅，这个时候flask.Blueprint(蓝图)就派上用场了
什么是蓝图？

一个蓝图定义了可用于单个应用的视图，模板，静态文件等等的集合。

我什么时候会用到蓝图？

蓝图的杀手锏是将你的应用组织成不同的组件，比如把这里的admin，user相关的视图方法分为两个组件，一个是admin组件，一个是user组件.这时我们可以
创建两个蓝图实现这两个独立的组件.

```