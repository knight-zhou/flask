#!/usr/bin/env python
#coding:utf-8
# 把view拆分成两个模块分别写功能
#要想创建一个蓝图对象，你需要import flask.Blueprint()类并用参数name和import_name初始化。
# import_name通常用__name__，一个表示当前模块的特殊的Python变量，作为import_name的取值。

from flask import Blueprint,render_template,request

#蓝图的实例对象
admin = Blueprint('admin',__name__)

@admin.route('/index/')
def index():
    return u"这是蓝图中的admin index页面"

@admin.route('/show')
def show():
    return u'这是蓝图中的admin show页面'

@admin.route('/add')
def add():
    return u'这是蓝图中的admin add页面'