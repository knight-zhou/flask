#!/usr/bin/env python
#coding:utf-8
from flask import Blueprint, render_template, redirect

#实例化
user = Blueprint('user',__name__)

@user.route('/index')
def index():
    return u'这是蓝图中的user首页'

@user.route('/add')
def add():
    return u'这是蓝图中user add页面'

@user.route('/show')
def show():
    return u'这是蓝图中user show页面'