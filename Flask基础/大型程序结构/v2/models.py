# coding:utf-8
from app import db

class Proms_alert(db.Model):
    __tablename__ = 'proms_alert'
    id = db.Column(db.INTEGER,primary_key=True)
    status = db.Column(db.String(255))
    ##
    alertname = db.Column(db.String(255))
    app = db.Column(db.String(255))
    group = db.Column(db.String(255))
    hostname = db.Column(db.String(255))
    instance = db.Column(db.String(255))
    job = db.Column(db.String(255))
    serverity = db.Column(db.String(255))
    ##
    description = db.Column(db.String(255))
    summary = db.Column(db.String(255))

    startsAt = db.Column(db.DateTime)
    endsAt = db.Column(db.DateTime)

    def __init__(self,status,alertname,group,hostname,instance,job,app,serverity,description,startsAt,endsAt):
        self.status=status
        self.alertname = alertname
        self.app = app
        self.group = group
        self.hostname = hostname
        self.instance = instance
        self.job = job
        self.serverity = serverity
        self.description = description

        self.startsAt=startsAt
        self.endsAt=endsAt






## 右键创建表
db.create_all()