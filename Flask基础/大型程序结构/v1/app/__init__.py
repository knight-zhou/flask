#!/usr/bin/env python
#coding:utf-8
#包初始化

from flask import Flask
app = Flask(__name__)
app.config.from_object("config") #从config.py读入配置


##这个import语句放在这里, 防止views, models import发生循环import
#不加下面之一段为 报错:
#The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.
from app import views,models

