#!/usr/bin/env python
#coding:utf-8
#运用Flask-Script为Flask编写服务器脚本, 产生类似Django的运行方式

from flask_script import Manager,Server
from app import app

manager = Manager(app)
manager.add_command("runserver",Server(host="0.0.0.0",port=7070,use_debugger=True))

if __name__=='__main__':
    manager.run()

