#!/usr/bin/env python
#coding:utf-8
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import *
from sqlalchemy.sql import select
from sqlalchemy import text

db = 'mysql://knight:knight@192.168.30.143/test'
engine = create_engine(db, echo=True)
metadata=MetaData()


user=Table('user',metadata,
           Column('id',Integer,primary_key=True),
           Column('name',String(20)),
           Column('sex',String(10)),
           )

address=Table('address',metadata,
           Column('id',Integer,primary_key=True),
           Column('user_id',String(20)),
           Column('email_address',String(10),nullable=False),
           )

#执行创建过程
# metadata.create_all(engine)
conn = engine.connect()

#插入数据,不需要事务的属性查看变更
# conn.execute(user.insert(),[dict(name='jack',sex='man'),dict(name='wendy',sex='women')])

#全表查询数据的操作
s = select([user])
result=conn.execute(s)

#迭代输出数据
for row in result:
    print row

#连表查询
s = select([user,address]).where(user.c.id==address.c.user_id)
for row1 in result:
    print row1


