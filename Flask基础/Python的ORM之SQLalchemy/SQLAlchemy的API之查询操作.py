#!/usr/bin/env python
#coding:utf-8
#sqlalchemy 的简单API操作

import sqlalchemy
from sqlalchemy import Column, Integer, String, DateTime, TIMESTAMP, DECIMAL
from sqlalchemy.sql.expression import text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


conn = 'mysql://knight:knight@192.168.30.143/test'
engine = create_engine(conn, echo=True)
Base = declarative_base()

#继承base类
class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    fullname = Column(String(100))

#如果有表会自动忽略一下的创建表的操作
# Base.metadata.create_all(engine)
ed_user=User(name='knight',fullname="Long Zhou")
print (ed_user)

#实例化session会话类
Session=sessionmaker(bind=engine)
session=Session()
session.add(ed_user)

#查询语句
our_user=session.query(User).filter_by(name='ed').first()
#select * from users where name='ed' limit 1;
'''
#插入数据
session.add_all([
    User(name='a',fullname='a 1'),
    User(name='b',fullname='b 2'),
    User(name='c',fullname='c 3'),
])

#像事务一样进行commit提交
session.commit()

'''
#where查询
print session.query(User).filter_by(name='a').first()

#in查询操作
for row in session.query(User).filter(User.name.in_(['knight'])):
    print row

#count查询统计
print (session.query(User).count())
