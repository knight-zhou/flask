#!/usr/bin/env python
#coding:utf-8

from sqlalchemy import *

conn = 'mysql://knight:knight@192.168.30.143/test'

#创建engine实例
db=create_engine(conn,echo=True)

#创建元数据
"""
什么是元数据？元数据就是描述数据的数据，
举个简单的例子，小明身高170cm，体重50kg，性别男。
其中身高，体重，性别就是元数据。
当我们创建好连接引擎以后可以通过这个引擎抓取元数据。
"""
metadata=MetaData(db)

table=Table('mytable',metadata,autoload=True)
i = table.insert()
i.execute(id="3",name="long")

