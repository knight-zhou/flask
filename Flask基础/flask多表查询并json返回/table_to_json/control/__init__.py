# coding:utf-8
## 初始化flask以及连接数据库
from flask import Flask
from settings import Config
from flask_sqlalchemy import SQLAlchemy

##
app = Flask(__name__)

app.config.from_object(Config)
db=SQLAlchemy(app)


### 一些控制器函数加进来,不然控制器的代码无法生效
from control import index