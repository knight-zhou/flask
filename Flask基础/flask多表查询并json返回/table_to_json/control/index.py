# coding:utf-8
from control import app
from models import Proms_alert,Proms_labels,Proms_annotations
import json

# 定义最终的构造结果


@app.route('/')
def index():
    return "hello world"

@app.route('/tt')
def tt():
    return "hello test"

@app.route('/list')
def list():
     #
    proms_labels=Proms_labels.query.filter_by(proms_alert_id=14).first()
    res_labels={"app":proms_labels.app,"hostname":proms_labels.hostname}
    ##
    proms_annotations=Proms_annotations.query.filter_by(proms_alert_id=14).first()
    res_annotations={"summary":proms_annotations.summary,"description":proms_annotations.description}

    ###
    proms_alert = Proms_alert.query.filter_by(id=14).first()
    res = {"status":proms_alert.status,"labels":res_labels,"startsAt":str(proms_alert.startsAt),"annotations":res_annotations,"endsAt":str(proms_alert.endsAt)}
    res_json=json.dumps(res)
    return res_json