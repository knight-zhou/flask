# coding:utf-8
from control import db

class Proms_alert(db.Model):
    __tablename__ = 'proms_alert'
    id = db.Column(db.INTEGER,primary_key=True)
    status = db.Column(db.String(255))
    startsAt = db.Column(db.DateTime)
    endsAt = db.Column(db.DateTime)
    def __init__(self,status,startsAt,endsAt):
        self.status=status
        self.startsAt=startsAt
        self.endsAt=endsAt



class Proms_annotations(db.Model):
    __tablename__ = 'proms_annotations'
    id = db.Column(db.INTEGER,primary_key=True)
    description = db.Column(db.String(255))
    summary = db.Column(db.String(255))
    proms_alert_id = db.Column(db.INTEGER)

    def __init__(self, description, summary,proms_alert_id):
        self.description=description
        self.summary=summary
        self.proms_alert_id=proms_alert_id



class Proms_labels(db.Model):
    __tablename__ = 'proms_labels'
    id = db.Column(db.INTEGER,primary_key=True)
    app = db.Column(db.String(255))
    group = db.Column(db.String(255))
    serverity = db.Column(db.String(255))
    instance = db.Column(db.String(255))
    hostname = db.Column(db.String(255))
    proms_alert_id = db.Column(db.INTEGER)

    def __init__(self, app, group, serverity,instance,hostname,proms_alert_id):
        self.app=app
        self.group=group
        self.serverity=serverity
        self.instance=instance
        self.hostname=hostname
        self.proms_alert_id=proms_alert_id



## 右键创建表
db.create_all()