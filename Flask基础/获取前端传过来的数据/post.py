#coding:utf-8
from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/")
def hello():
    return "hello world!"

@app.route("/send", methods=["POST"])
def send():
    params = request.json
    print(type(params))
    print(params)
    return "ok"

if __name__ == '__main__':
    app.run()