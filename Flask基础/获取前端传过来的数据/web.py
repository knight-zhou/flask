#coding:utf-8
from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/")
def hello():
    return "hello world!"

@app.route("/send", methods=["POST"])
def send():
    username = request.form['name']
    postForm = request.form
    print(type(postForm))
    return "ok"

if __name__ == '__main__':
    app.run()