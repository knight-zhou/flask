#!/usr/bin/env python3
#coding:utf-8

from flask import Flask
from flask_apscheduler import APScheduler

class Config(object):
    JOBS = [
        {
            'id': 'xxoo',
            'func': 'jobs2:xxoo',       # 文件名要和jobs2一致
            'args': '',          
            'trigger': 'interval',
            'seconds': 10
        }
    ]

    SCHEDULER_API_ENABLED = True


def xxoo():
    print ('定时跑任务.....')

if __name__ == '__main__':
    app = Flask(__name__)
    app.config.from_object(Config())

    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.start()

    app.run()