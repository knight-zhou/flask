#### 牛逼的VirtualEnv环境
作用:virtualenv虚拟环境为每个项目隔离了一套运行类库，不同的项目在各自的虚拟环境中使用不同的类库，避免了将所有类库都安装到系统环境中导致的不同项目需要不同（版本）类库的问题，项目与项目之间的类库依存不再成为问题

命令virtualenv就可以创建一个独立的Python运行环境，我们还加上了参数--no-site-packages，

这样，已经安装到系统Python环境中的所有第三方包都不会复制过来，这样，我们就得到了一个不带任何第三方包的“干净”的Python运行环境.

安装:
```
pip3 install virtualenv
pip install virtualenv

```
> 对于windows:
```
>cd project
>md mypro
>virtualenv env
>cd mypro\env\Scripts
> start activate
(env) C:\Users\Administrator\Desktop\tmp\mypro\env\Scripts>pip install redis
离开虚拟环境，使用deactivate命令：
>deactivate
```
> 对于Linux:
```
$mkdir mypro
$cd mypro
$/usr/local/python2.7.11/bin/virtualenv --no-site-packages venv
$source venv/bin/activate
(venv) [root@vm11 mypro]$ pip install redis

```

