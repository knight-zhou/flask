# coding:utf-8
from app import app
import json
from flask import request
from models import Proms_alert
from app import db


@app.route('/test')
def test():
    proms_laert = Proms_alert(status= "aa",alertname="bb")    # 可以不在model里定义init方法
    ## 提交更新数据
    db.session.add(proms_laert)
    db.session.commit()
    return "ok"


if __name__ == '__main__':
    app.run(host='0.0.0.0',debug="true",port=5001)
