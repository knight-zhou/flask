#!/usr/bin/env python
#coding:utf-8

from exts import db
from datetime import datetime
from werkzeug.security import generate_password_hash,check_password_hash

#创建用户模型
class Role(db.Model):
    __tablename__ = 'role'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    role = db.Column(db.String(50),nullable=False)
    username = db.Column(db.String(50),nullable=False)

    ## 拦截用户名和密码
    def __init__(self,*args,**kwargs):
        role = kwargs.get('role')
        username = kwargs.get('username')

        self.username = username
        self.role = role




