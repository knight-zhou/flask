#!/usr/bin/env python
#coding:utf-8


from flask import Flask,render_template,request,redirect,url_for,session,g
import config
from models import User,Question,Answer,Tb_num
from exts import db
from decorators import login_required
from sqlalchemy import or_
import json

app = Flask(__name__)
app.config.from_object(config)

##切记不要忘记了这个初始化
db.init_app(app)


@app.route('/')
def index():
    # context = {'nums':Tb_num.query.all()}
    nums = Tb_num.query.all()
    shu = len(nums)
    # shu = Tb_num.query.count()    ##  也可以查数据库的纪录数
    new_n= []
    for i in range(shu):
        print nums[i]
        new_n.append(nums[i].num)
    new_n_str = str(new_n)
    new_n_strr = str(new_n).replace('L','')
    # xx = "[5, 20, 36, 10, 10, 20]"
    return render_template('ech.html',nu = new_n_strr)


if __name__=='__main__':
    app.run(debug=True)
