  
function zhux(nu,id){
// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('main'));

        // var data=[5, 20, 36, 10, 10, 20]
        var data= nu;
        var id = id;
        
        
        // 指定图表的配置项和数据
        var option = {
            title: {
                text: 'ECharts 入门示例'
            },
            tooltip: {},
            legend: {
                data:['销量']
            },
            xAxis: {
                data: id
            },
            yAxis: {},
            series: [{
                name: '销量',
                type: 'bar',
                data: data
            }]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
  }
  