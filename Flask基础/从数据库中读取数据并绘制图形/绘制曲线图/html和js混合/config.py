#!/usr/bin/env python
#coding:utf-8
import os

DEBUG=True
SECRET_KEY=os.urandom(24)

DIALECT = 'mysql'
DRIVER='mysqldb'

USERNAME='root'
PASSWORD='root'
HOST = 'localhost'
PORT = '3306'
DATABASE = 'flaskqa'

DB_URI="{}+{}://{}:{}@{}:{}/{}?charset=utf8".format(DIALECT,DRIVER,USERNAME,PASSWORD,HOST,PORT,DATABASE)

SQLALCHEMY_DATABASE_URI=DB_URI

#去掉警告
SQLALCHEMY_TRACK_MODIFICATIONS =False

