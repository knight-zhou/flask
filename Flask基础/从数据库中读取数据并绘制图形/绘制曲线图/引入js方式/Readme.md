## 爆出的一个问题的解决方法
后端:
```
@app.route('/')
def index():
    # context = {'nums':Tb_num.query.all()}
    nums = Tb_num.query.all()
    return render_template('index.html',nu = nums)
```
前端:
```
<html>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <body>
  
      {{ nu[1] }}
        
    </body>
</html>
```
结果前端显示:
```
<models.Tb_num object at 0x0000000004139470> 
```
### 原因
```
all()返回的是对象的地址,后端 封成json格式,前端再解一次
http传输都是字符串 不能是对象
```
### 解决
```
<html>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <body>
      {{ nu[0].num }}
      {{ nu[1].num }}
      {{ nu[2].num }}
      {{ nu[3].num }}
      {{ nu[4].num }}
    </body>
</html>
```
### 但是数据库里的数据纪录很多 我最终要把这个列表的数据放在变量里面传到前端
