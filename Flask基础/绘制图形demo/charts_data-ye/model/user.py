#-*- coding=utf-8 -*-
# from model import cur
import logging

class Order(object):
    
    def __init__(self):
        pass

    def get_user_order(self):
        '''
        按用户分组统计订单
        '''
        __sql = '''
        SELECT count(order_id) as order_count,user_id FROM test.t_order group by user_id order by order_count desc limit 15;
        '''
        cur.execute(__sql)
        result = cur.fetchall()
        desc = cur.description
        data = []
        for item in result:
            _item = {}
            for i in range(0, len(item)):
                _item[desc[i][0]] = str(item[desc[i][0]])
                data.append(_item)
        return data
    def get_data(self):
        __list = [
{"x":"2018-03-23","y1":739,"y2":1726,"y3":9572,"y4":5},
{"x":"2018-03-24","y1":347,"y2":1612,"y3":9039,"y4":3},
{"x":"2018-03-25","y1":437,"y2":1372,"y3":7440,"y4":0},
{"x":"2018-03-26","y1":437,"y2":1372,"y3":7440,"y4":0},
{"x":"2018-03-27","y1":866,"y2":1781,"y3":11092,"y4":6},
{"x":"2018-03-28","y1":819,"y2":1831,"y3":11619,"y4":10},
{"x":"2018-03-29","y1":751,"y2":1721,"y3":10397,"y4":8}

]
        return __list