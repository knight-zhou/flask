from flask import render_template,jsonify,request
from user import user
from model.user import Order
  
@user.route('/')
def index():
    
    return render_template('user/index.html')

@user.route('/list')
def list():
    order = Order()
    order_list = order.get_data()
    return jsonify({"data":order_list}), 201