#-*-coding=utf-8-*-
from flask import Flask
from user import user
from index import index

app = Flask(
    __name__,
    template_folder = 'templates', 
    static_folder = 'static', 
    #static_url_path='/opt/auras/static',
)

# 项目注册模块，并指定url前缀
app.register_blueprint(index) 
app.register_blueprint(user,url_prefix='/u') 


