# coding:utf-8
from flask import Flask
from flask_restful import Api
from views import *

app = Flask(__name__)
api = Api(app)

#路由设置
api.add_resource(TodoList, '/list')
api.add_resource(Todo, '/list/<todo_id>')
api.add_resource(TodoDel,'/del/<todo_id>')
api.add_resource(TodoAdd,'/add/<todo_key>/<todo_value>')

if __name__ == '__main__':
    app.run(debug=False)