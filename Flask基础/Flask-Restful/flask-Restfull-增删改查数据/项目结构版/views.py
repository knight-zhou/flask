# coding:utf-8
from flask import Flask, request
from flask_restful import Resource, Api,abort


## 定义原始字典，方便以后json输出
todos = {"name":"abc","app_path":"/usr/local"}

def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in todos:
        abort(404, message="Todo {} doesn't exist".format(todo_id))   ##不存在报404

# 根据条件查数据库操作
class Todo(Resource):
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return todos[todo_id]    # 查询字典元素

# 查数据库完整列表
class TodoList(Resource):
    def get(self):
        return todos

#删除数据库中的元素
class TodoDel(Resource):
    def get(self,todo_id):
        del todos[todo_id]   #删除字典元素
        return todos

# 增加数据库记录
class TodoAdd(Resource):
    def get(self,todo_key,todo_value):
        todos[todo_key]=todo_value
        return todos

## updae 可以改这里忽略