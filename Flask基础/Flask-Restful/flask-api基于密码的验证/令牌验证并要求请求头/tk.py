# coding:utf-8
from flask import Flask,g
from flask_httpauth import HTTPTokenAuth

app = Flask(__name__)
#初始化HTTPTokenAuth对象时，我们传入了”scheme=’yeye'”。这个scheme，就是我们在发送请求时，
# 在HTTP头”Authorization”中要用的scheme字段。
auth = HTTPTokenAuth(scheme='yeye')

tokens = {
    "secret-token-1": "John",
    "secret-token-2": "Susan"
}

# 验证令牌是否合法
@auth.verify_token
def verify_token(token):
    g.user = None
    if token in tokens:
        g.user = tokens[token]   #通过Token获取了用户信息，并保存在全局变量g中，这样视图中可以获取它
        return True
    return False


##视图函数
@app.route('/')
@auth.login_required
def index():
    return "Hello, %s!" % g.user

if __name__ == '__main__':
    app.run()