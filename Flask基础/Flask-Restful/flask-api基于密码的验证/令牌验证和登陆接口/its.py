# coding:utf-8
from flask import Flask,g
from flask_httpauth import HTTPTokenAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
import json

# 访问: curl http://127.0.0.1:5000/login
app = Flask(__name__)
auth = HTTPTokenAuth(scheme='yeye')   # 初始化HTTPTokenAuth对象时,在HTTP头”Authorization”中要用的scheme字段

#
app.config['SECRET_KEY'] = 'secret key here'

#这里实例化了一个针对JSON的签名序列化对象serializer，它是有时效性的，30分钟后序列化后的签名即会失效
serializer = Serializer(app.config['SECRET_KEY'], expires_in=1800)

# 定义数据库或者其他介质中的用户列表，当然你可以写多个
users = ['tom']

@auth.verify_token
def verify_token(token):
    g.user = None
    try:
        data = serializer.loads(token)
    except:
        return False
    if 'username' in data:
        g.user = data['username']
        return True
    return False

##视图函数
@app.route('/api')
@auth.login_required
def index():
    return "Hello, %s!" % g.user


#一般是post请求，剩下自己去扩展，这里提供思路	
@app.route('/login')
def login():
    token = serializer.dumps({'username': users[0]})
    token = str(token, encoding='utf-8')  #bytes类型的数据就把它转化成str类型
    res_data = {"token":token}
    return json.dumps(res_data)

if __name__ == '__main__':
    app.run()