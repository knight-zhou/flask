# coding:utf-8
from flask import Flask,render_template,request
import time,pymysql

db = pymysql.connect(host="localhost",user="root",passwd="root",db="memory")
db.autocommit(True)
cur = db.cursor()
app=Flask(__name__)
import json

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/data')
def data():
    sql = 'select * from memory'
    cur.execute(sql)
    arr = []
    for i in cur.fetchall():
        arr.append([i[1]*1000,i[0]])
    return json.dumps(arr)

if __name__=='__main__':
    app.run(host='0.0.0.0',port=9092,debug=True)