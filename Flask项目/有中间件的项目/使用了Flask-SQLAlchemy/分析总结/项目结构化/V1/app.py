from flask import Flask
import config
from exts import db
from models import User

app = Flask(__name__)
app.config.from_object(config)

## 切记要初始化
db.init_app(app)

@app.route("/charu")
def charu():
    admin = User(username='admin', email='admin@example.com')
    db.session.add(admin)
    db.session.commit()
    return "插入成功....."

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port=5000)





