# coding:utf-8
from models import Proms_alert
from app import db
from common.timechange import utc2local
# data={'receiver': 'devops\\.wechatmail', 'status': 'resolved', 'alerts': [{'status': 'resolved', 'labels': {'alertname': 'tcp_port_check failed', 'app': 'tms-admin', 'group': 'tms', 'hostname': 'hwy-hn1-transport-admin-prd-0', 'instance': '172.19.192.34:80833', 'job': 'tcp_port_check', 'serverity': 'critical'}, 'annotations': {'description': 'tms的tms-admin tcp检测失败,当前probe_success的值为0', 'summary': 'tms组的应用 tms-admin 端口检测不通'}, 'startsAt': '2019-10-29T20:45:06.631494166+08:00', 'endsAt': '2019-10-29T20:48:21.631494166+08:00', 'generatorURL': 'http://hwy-hn1-carsales-baseservice:9090/graph?g0.expr=probe_success%7Bjob%3D%22tcp_port_check%22%7D+%3D%3D+0&g0.tab=1'}], 'groupLabels': {'alertname': 'tcp_port_check failed'}, 'commonLabels': {'alertname': 'tcp_port_check failed', 'app': 'tms-admin', 'group': 'tms', 'hostname': 'hwy-hn1-transport-admin-prd-0', 'instance': '172.19.192.34:80833', 'job': 'tcp_port_check', 'serverity': 'critical'}, 'commonAnnotations': {'description': 'tms的tms-admin tcp检测失败,当前probe_success的值为0', 'summary': 'tms组的应用 tms-admin 端口检测不通'}, 'externalURL': 'http://hwy-hn1-carsales-baseservice:9093', 'version': '4', 'groupKey': '{}:{alertname="tcp_port_check failed"}'}

def insert(data):
    aa = data['alerts']
    bb = aa[0]
    ## 先插入第一张表
    # proms_laert = Proms_alert(bb['status'], utc2local(bb['startsAt']), utc2local(bb['endsAt']))
    proms_laert = Proms_alert(
        bb['status'],
        bb['alertname'],
        bb['app'],
        bb['group'],
        bb['hostname'],
        bb['instance'],
        bb['job'],
        bb['serverity'],
        bb['description'],
        utc2local(bb['startsAt']),
        utc2local(bb['endsAt'])
    )

    ## 提交更新数据
    db.session.add(proms_laert)
    db.session.commit()
