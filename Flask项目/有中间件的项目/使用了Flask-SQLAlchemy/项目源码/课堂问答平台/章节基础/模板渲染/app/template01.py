#!/usr/bin/env python
#coding:utf-8

from flask import  Flask,render_template
app=Flask(__name__)


# 使用关键字参数
@app.route('/')
def index():
    context={'username':u'天龙八部','sex':u'男','age':18}
    return render_template('index.html',**context)
    # return render_template('index.html',username=u'天龙八部',sex='man',age=18)


if __name__=="__main__":
    app.run(host='0.0.0.0',debug=True)

