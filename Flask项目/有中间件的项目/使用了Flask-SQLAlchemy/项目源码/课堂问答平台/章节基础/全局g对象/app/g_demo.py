#!/usr/bin/env python
#coding:utf-8
from flask import Flask,g,render_template,request
from utils import login_log

app = Flask(__name__)


@app.route('/')
def index():
    return ""

@app.route('/login/',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        username = request.form.get('username')
        password = request.form.get('password')

        if username == 'knight' and password=='123456':
            #就认为用户名和密码正确
            # 绑定 g 对象
            g.username='knight'
            g.ip='xx'

            login_log()
            return "恭喜登陆成功"
        else:
            return u"您的用户名和密码错误"

    return "daf"



if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)
