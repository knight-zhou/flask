## for 循环遍历 列表和字典
1. 字典的遍历,语法和python 一样,可以使用 items() keys() values() iteritems() 等等
```
   {% for k,v in user.items() %}
            <p>{{ k }}:{{ v }}</p>
    {% endfor %}

```

2.列表的遍历:语法和python一样
```
 {% for website in websites %}
   <p> {{ website }}</p>
  {% endfor %}

```


