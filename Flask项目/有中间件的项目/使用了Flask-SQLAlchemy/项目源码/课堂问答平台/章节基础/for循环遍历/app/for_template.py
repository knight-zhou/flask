#!/usr/bin/env python
#coding:utf-8

from flask import  Flask,render_template
app=Flask(__name__)

@app.route('/')
def index():
    user={
        'username':u'天龙八部',
        'age':18
          }

    websites = {'baidu.com','google.com'}
# 字典遍历
    # for k,v in user.items():
    #     print k
    #     print v

    return render_template('index.html',user=user,websites=websites)


if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)

