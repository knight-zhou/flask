#!/usr/bin/env python
#coding:utf-8
from flask import Flask,render_template,request

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/search/')
def search():
    # print request.args
    q = request.args.get('q')
    return "用户提交的查询参数是: {0} ".format(q)

#默认是的视图函数智能采用get请求
#如果要用post，要写明
@app.route('/login/',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        username = request.form.get('username')
        password = request.form.get('password')
        # print username
        return username + " "+ password

    # return render_template('login.html')




if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)
