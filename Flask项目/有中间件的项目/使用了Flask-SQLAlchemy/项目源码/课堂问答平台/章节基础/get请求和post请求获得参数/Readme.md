### get和post请求获取参数
1. get请求通过 `flask.request.args` 来获取

2. post请求通过`flask.request.form` 来获取

3.post请求在模板中要注意几点:

* input 标签中 要写name来标识这个value的key，方便后台获取
* 在写form表单的时候 要指定 method='post' ，并且要指定 action='login'

4. 示例代码
```

<form action="{{ url_for('login') }}" method="post">

<table>
    <tbody>
    <tr>
        <td>用户名:</td>
        <td><input type="text" placeholder=" 请输入用户名" name="username"></td>
    </tr>

    <tr>
        <td>密码:</td>
        <td><input type="text" placeholder=" 请输入密码" name="password"></td>
    </tr>

        <tr>
            <td></td>
            <td><input type="submit" value="登陆"></td>

        </tr>

    </tbody>

</table>

 </form>
```
