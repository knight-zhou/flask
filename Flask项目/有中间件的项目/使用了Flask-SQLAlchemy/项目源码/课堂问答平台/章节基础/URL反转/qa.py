#!/usr/bin/env python
#coding:utf-8

from flask import Flask,url_for
app = Flask(__name__)


# 通过视图函数得到指向的url
@app.route('/')
def index():
    print url_for('my_list')
    print url_for('article',id='abc')
    return "////..............."

@app.route('/list/')
def my_list():
    return 'list'


@app.route('/article/<id>/')
def article(id):
    return u"您请求的id是 {0}".format(id)



if __name__== '__main__':
    app.run(debug=True)


