#!/usr/bin/env python
#coding:utf-8

from flask import  Flask,render_template
app=Flask(__name__)



@app.route('/')
def index():
    class Person(object):
        name=u"天龙八部"
        age=18
    p=Person()

    context={'username':u'天龙八部',
             'sex':u'男',
             'age':18,
             'person':p,
             'website':{'google':'www.google.com'}
             }
    return render_template('index.html',**context)


if __name__=="__main__":
    app.run(host='0.0.0.0',debug=True)

