## 模板传参
* 如果只有一个或者少量参数,直接在`render_template` 函数中添加关键字参数就可以了
* 如果有多个参数的时候,那么可以吧所有的参数放在字典中,然后在`render_template` 中，使用两个星号.
* 使用两个星号 把字典转换成关键字参数传递进去，这样代码更方便管理和使用

* 在模板中，如果使用一个变量，语法是{{ params }}
* 访问模型中的属性或者字典，可以通过{{ params.proerty }}的形式，或者使用 {{ params['age'] }}