### Flask-Sqlalchemy 的模型与表映射

1. 模型需要继承自`db.Model`,然后需要映射到表中的属性,必须写成`db.Column`的数据类型

2. 数据类型：

* `db.Inter`代表的是整形
* `db.String`代表的是varhchar,需要指定最长的长度
* `db.Text` 代表的是`text`

3. 其他参数:
* `primary_key`： 代表字段设置为主键
* `autoincrement`:  代表 自增长
* `nullable`:  是否为空

4. 最后需要调用`db.create_all` 来讲模型真正的创建数据库中.