#!/usr/bin/env python
#coding:utf-8
from flask import  Flask
from  flask_sqlalchemy import SQLAlchemy

import config

app=Flask(__name__)
app.config.from_object(config)
db = SQLAlchemy(app)

#创建表和模型的映射
'''
article表:
create table article(
    id int primary key autoincrement,
    title varchar(100) not null,
    content text not null,
)

'''
#默认继承Model类
class Article(db.Model):
    __tablename__='article'   ##指定表名
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    title= db.Column(db.String(100),nullable=False)
    content = db.Column(db.Text,nullable=False)

#表名映射到数据库中
db.create_all()









@app.route('/')
def index():
    return "index"



if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)

