#!/usr/bin/env python
#coding:utf-8
from flask_script import Manager
from app import app
from app.views import *

manage = Manager(app)

@manage.command
def init():
    print "数据库初始化完成..."


if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)

