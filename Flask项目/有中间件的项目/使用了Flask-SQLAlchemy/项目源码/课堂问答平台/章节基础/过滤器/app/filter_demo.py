#!/usr/bin/env python
#coding:utf-8
#http://userhead.szhomeimg.com/userface/100/003/177/3177210.jpg
from flask import  Flask,render_template
app=Flask(__name__)

#使用过滤器,类似于linux中的管道
@app.route('/')
def index():
    comments = [
        {
            'user':u'天龙八部',
            'content':'kkk',
        },
        {
            'user':u'少帅',
            'content':"xxx"
        },
        {
            'user':u'奥巴马De顾问',
            'content':'ooo'

        }
    ]

    return render_template('index.html',comments=comments)
    # avatar='http://userhead.szhomeimg.com/userface/100/003/177/3177210.jpg')


if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)

