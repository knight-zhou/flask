#!/usr/bin/env python
#coding:utf-8
from flask import Flask,render_template,request,session,redirect,url_for,g
import os

app = Flask(__name__)
app.config['SECRET_KEY']=os.urandom(24)

@app.route('/')
def hello_world():
    print 'index'
    return render_template('index.html')   # 没有传入任何字典，但是前台页面可以显示username

@app.route('/login/',methods=['GET','POST'])
def login():
    print "login"    #说明是钩子函数之前再执行login的
    if request.method == 'GET':
        return render_template('login.html')
    else:
        username = request.form.get('username')
        password= request.form.get('password')
        if username == 'knight' and password=='123456':
            session['username'] = 'knight'
            return u'登陆成功'
        else:
            return u"用户名和密码错误"

@app.route('/edit/')
def edit():
    # if session.get('username'):
    # if hasattr(g,'username'):
    #     return render_template('edit.html')
    # else:
    #     return redirect(url_for('login'))
    return render_template('edit.html',usename='knight')




#before_request: 在请求之前执行
#before_request 是在视图函数之前执行的
#before_request 这个函数只是一个装饰器,他可以把需要设置为钩子函数的代码放到视图函数之前执行
# 类似于django的装饰器
# 在每一个视图函数的request之前都会执行钩子函数

@app.before_request
def my_before_request():
    if session.get('username'):
        g.username=session.get('username',username='knight')


#上下文处理中返回中的字典，在所有页面中都可用的
@app.context_processor
def my_context_processor():
    # username= session.get('username')
    # if username:
    #     return {'username':username}
    return {'username':'knight'}




if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)
