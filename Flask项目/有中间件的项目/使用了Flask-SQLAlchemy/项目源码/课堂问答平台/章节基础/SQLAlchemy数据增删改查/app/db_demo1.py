#!/usr/bin/env python
#coding:utf-8
from flask import  Flask
from  flask_sqlalchemy import SQLAlchemy

import config

app=Flask(__name__)
app.config.from_object(config)
db = SQLAlchemy(app)

#创建表和模型的映射
'''
article表:
create table article(
    id int primary key autoincrement,
    title varchar(100) not null,
    content text not null,
)

'''
#默认继承Model类
class Article(db.Model):
    __tablename__='article'   ##指定表名
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    title= db.Column(db.String(100),nullable=False)
    content = db.Column(db.Text,nullable=False)

#表名映射到数据库中
db.create_all()



@app.route('/')
def index():
    # # 增加
    # article = Article(title=u'aaa',content='bbb')
    # db.session.add(article)                                      ##所有的db增删该查都在session中进行
    # # 事务
    # db.session.commit()

    # 查
    res = Article.query.filter(Article.title=='aaa').all()
    # res = Article.query.filter(Article.title=='aaa').first()
    # article1 = res[0]
    # print article1.title
    # print article1.content

    # # 改:
    # #1. 先把你要更改的数据查找出来
    # res = Article.query.filter(Article.title == 'aaa').first()
    # #2. 把这条要更改的数据查找出来
    # res.title = 'new title'
    #
    # #3.做事务的提交
    # db.session.commit()

    # 删:
     # 1.先把删除的数据查找出来
    res = Article.query.filter(Article.title == 'aaa').first()
    # 2. 把这条数据删除掉
    db.session.delete(res)
    # 3. 事务提交
    db.session.commit()


    return 'Hello wolrd'



if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)

