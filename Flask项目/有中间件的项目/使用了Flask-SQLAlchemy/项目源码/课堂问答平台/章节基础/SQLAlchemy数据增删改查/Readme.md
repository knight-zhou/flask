### Flask-Sqlalchemy 的数据的增 删 改 查
1. 增:
```
article = Article(title=u'aaa',content='bbb')
db.session.add(article)
#事务
db.session.commit()
```

2. 查:
```
 # 查
 res = Article.query.filter(Article.title=='aaa').all()
 res = Article.query.filter(Article.title=='aaa').first()
 article1 = res[0]
 print article1.title
 print article1.content

```
3.改
```
1. 先把你要更改的数据查找出来
 res = Article.query.filter(Article.title == 'aaa').first()

 2. 把这条要更改的数据查找出来
 res.title = 'new title'

 3.做事务的提交
 db.session.commit()
```
4. 删:

