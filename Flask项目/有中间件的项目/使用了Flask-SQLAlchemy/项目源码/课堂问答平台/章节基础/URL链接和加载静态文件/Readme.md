### url链接：使用 `url_for(视图函数名称)`可以反转成url

### 加载静态文件：
1. 语法: `url_for('static',filename='路径')`

2. 静态文件,flask 会从`static` 文件夹中开始寻找,所以不需要写'static'这个路径了.

3. 可以加载css 文件,可以加载 js文件，还有image文件.

