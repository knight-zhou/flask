### Flask-Sqlalchemy 的使用
1. 初始化设置数据库配置信息
* 初始化flask_sqlalchemy中的SQLchemy进行初始化
```
from  flask_sqlalchemy import SQLAlchemy
app=Flask(__name__)
db = SQLAlchemy(app)
```
2. 设置配置信息：在config.py文件中添加配置信息
```
DIALECT = 'mysql'
DRIVER='mysqldb'
USERNAME='knight'
PASSWORD='knight'
HOST = '192.168.30.143'
PORT = '3306'
DATABASE = 'flaskdb'

SQLALCHEMY_DATABASE_URI="{}+{}://{}:{}@{}:{}/{}".format(DIALECT,DRIVER,USERNAME,PASSWORD,HOST,PORT,DATABASE)

#去掉警告
SQLALCHEMY_TRACK_MODIFICATIONS =False
```
3. 在主app文件中，添加配置文件:
```
app=Flask(__name__)
app.config.from_object(config)
db = SQLAlchemy(app)
```
4. 做测试,看有没有问题
```
db.create_all()
```
如果没有报错 说明配置没有问题