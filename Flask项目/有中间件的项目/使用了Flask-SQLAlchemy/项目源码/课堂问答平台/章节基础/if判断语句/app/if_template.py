#!/usr/bin/env python
#coding:utf-8
"""
笔记:
1.语法:
    {% if xxx %}
    {% else %}
    {% endif %}

2.if 的使用,可以和python中相差无几

"""


from flask import  Flask,render_template
app=Flask(__name__)



## 一下是if方面的内容

@app.route('/<is_login>/')
def index(is_login):
    if is_login=='1':
        user = {
            'username':u'少帅',
            'age':19

        }
        return render_template('index.html',user=user)
    else:
        return render_template('index.html')


if __name__=='__main__':
    app.run(host='0.0.0.0',debug=True)

