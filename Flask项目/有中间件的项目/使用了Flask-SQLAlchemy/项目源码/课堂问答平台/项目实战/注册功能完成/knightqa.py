#!/usr/bin/env python
#coding:utf-8


from flask import Flask,render_template,request,redirect,url_for
import config
from models import User
from exts import db

app = Flask(__name__)
app.config.from_object(config)



##切记不要忘记了这个初始化
db.init_app(app)

@app.route('/')
def index():
    return render_template('index.html')

#登录页面
@app.route('/login/',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        pass

@app.route('/regist/',methods=['GET','POST'])
def regist():
    if request.method == 'GET':
        return render_template('regist.html')
    else:
        telephone = request.form.get('telephone')
        username = request.form.get('username')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')

        # 手机号码验证,如果被注册了,就不能再注册了
        user = User.query.filter(User.telephone == telephone).first()

        if user:
            return u"该手机号码已经被注册，请更换手机号码!"
        else:
            if password1 != password2:
                return u"两次密码不相等 请核对"
            else:
                user = User(telephone=telephone, username=username, password=password1)
                db.session.add(user)
                db.session.commit()
                # 如果注册成功 就让页面跳转到登陆的页面
                return redirect(url_for('login'))


if __name__=='__main__':
    app.run()
