# coding:utf-8
from app import db
from sqlalchemy.dialects.mysql import TEXT

class Proms_alert(db.Model):
    __tablename__ = 'pk_user'
    id = db.Column(db.INTEGER,primary_key=True)
    email = db.Column(db.String(255))
    password = db.Column(db.String(255))
    sex = db.Column(db.String(255))
    birthday = db.Column(db.String(255))
    height = db.Column(db.INTEGER)
    current = db.Column(db.String(255))
    education = db.Column(db.String(255))
    province = db.Column(db.String(255))
    city = db.Column(db.String(255))
    county = db.Column(db.String(255))
    professional = db.Column(db.String(255))
    weight = db.Column(db.String(255))
    phone = db.Column(db.String(255))
    qq = db.Column(db.String(255))
    weixin = db.Column(db.String(255))
    income = db.Column(db.String(255))
    hobby = db.Column(db.String(255))
    character = db.Column(db.String(255))
    expect = db.Column(TEXT)
    myinfo = db.Column(TEXT)
    imgurl = db.Column(TEXT)
    cando = db.Column(TEXT)
    name = db.Column(db.String(255))
    regtime = db.Column(db.DateTime)
    lastlogintime = db.Column(db.DateTime)
    vip = db.Column(db.INTEGER)
    vipexptime = db.Column(db.INTEGER)


## 右键创建表
db.create_all()