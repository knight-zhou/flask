var _layer_load;
var NavData = [];

function LoadNavs(data) {
	var _htm = '';
	//console.log(data);
	for (var i = 0; i < data.length; i++) {
		_htm += '<li class="layui-nav-item' + (data[i]['delt'] ? ' layui-nav-itemed' : '') + '" onclick="NavGo(' + i + ',\'' +
			data[i]['href'] + '\',\'' + data[i]['tget'] + '\')"><a href="javascript:;">' + (data[i]['icon'] ?
				'<i class="layui-icon">' + data[i]['icon'] + '</i> ' : '') + data[i]['name'] + '</a></li>';
	}
	$('#nav-head').html(_htm);
	layui.element.render('nav');
}

function NavGo(id, _href, _tget) {
	if (_href) {
		//加载等待框
		_layer_load = layer.open({
			type: 3,
			shade: 0
		});
		window.open(_href, (_tget ? _tget : 'mainiframe'));
		return true;
	}
	var data = NavData[id]['navs'] || [],
		_htm = '';
	for (var i = 0; i < data.length; i++) {
		if (data[i]['navs']) {
			_htm += '<li class="layui-nav-item' + (data[i]['delt'] ? ' layui-nav-itemed' : '') + '"><a href="javascript:;">' + (
					data[i]['icon'] ? '<i class="layui-icon">' + data[i]['icon'] + '</i> ' : '') + data[i]['name'] +
				'</a><dl class="layui-nav-child">';
			for (var i2 = 0; i2 < data[i]['navs'].length; i2++) {
				_htm += '<dd onclick="NavGo(' + i2 + ',\'' + root_path + data[i]['navs'][i2]['href'] + '\',\'' + data[i]['navs'][i2]
					['tget'] +
					'\')" class="' + (data[i]['navs'][i2]['delt'] ? 'layui-this' : '') + '"><a href="javascript:;">' + (data[i]['navs']
						[i2]['icon'] ? '<i class="layui-icon">' + data[i]['navs'][i2]['icon'] + '</i> ' : '') + data[i]['navs'][i2][
						'name'
					] + '</a></dd>';
			}
			_htm += '</dl></li>';
		} else {
			_htm += '<li class="layui-nav-item" onclick="NavGo(' + i + ',\'' + data[i]['href'] + '\',\'' + data[i]['tget'] +
				'\')"><a target="mainiframe" href="javascript:;">' + (data[i]['icon'] ? '<i class="layui-icon">' + data[i]['icon'] +
					'</i> ' : '') + data[i]['name'] + '</a></li>';
		}
	}
	$('#nav-left').html(_htm);
	layui.element.render('nav', 'nav-left');
}

function vipframe(_t) {
	layer.open({
		type: 2,
		content: "index.php?c=app&a=panel:index&s=vip&do=" + _t,
		title: "VIP服务",
		area: ["100%", "100%"],
		move: false,
		resize: false
	});
}

function Recharge(checked) {
	AliOrWeixin_CloudPay('title=账户充值&amount=100&description=Yodati账户充值');
	return false;
	if (typeof(AliOrWeixin_CloudPay) == "undefined") {
		if (checked) {
			layer.open({
				type: 0,
				content: '<p>方式一：（推荐）</p><p>支付宝或微信扫码并备注您的用户名，充值将在1小时内到账。</p><p>方式二：</p><p>添加客服QQ:632827168或微信:puyuetian，进行人工充值。</p><br><p>收款二维码：</p><p style="width:128px;text-align:center;display:inline-block;margin-right:27px"><img src="template/default/img/alipay.jpg" alt=""><br>支付宝</p><p style="width:128px;text-align:center;display:inline-block"><img src="template/default/img/wxpay.jpg" alt=""><br>微信</p>',
				title: "充值方式",
				area: ['450px', '430px']
			});
		} else {
			layer.msg('<i class="_anim-loop">/</i>&nbsp;&nbsp;正在初始化，请稍后...', {
				time: 2700
			}, function() {
				Recharge(true);
			});
		}
	} else {
		//layer.closeAll();
		AliOrWeixin_CloudPay('title=账户充值&amount=100');
	}
}


function popupUserMenu(_v) {
	if (_v == 'tcdl') {
		layer.confirm('确认退出当前用户的登录吗？', {
			icon: 3,
			title: '退出'
		}, function(index) {
			$.getJSON(root_path + 'index.php/index/admin/login/check/logout', function(data) {
				if (data['state'] == 'ok') {
					layer.msg('用户成功退出', {
						icon: 1,
						time: 1500
					}, function() {
						location.href = root_path;
					});
				} else {
					layer.msg(data['datas']['msg'], {
						icon: 2,
						time: 1500
					});
					console.log(data);
				}
			});
			layer.close(index);
		});
		return false;
	}
	_mysetpage();
};

function _mysetpage() {
	//个人设置
	layer.tab({
		area: $(window).width() > 1000 ? ['60%', '70%'] : ['100%', '100%'],
		tab: [{
			title: '修改密码',
			content: $('#grsz-xgmm').html()
		}]
	});
}

function savemyset(This, type) {
	if ($(This).hasClass('layui-btn-disabled')) {
		return false;
	}
	var strs;
	var obj = $(This).parents('form:eq(0)');
	if (type == 'xgmm') {
		var nun = obj.find('input[name="newusername"]').val();
		var opw = obj.find('input[name="oldpassword"]').val();
		var npw = obj.find('input[name="newpassword"]').val();
		var npw2 = obj.find('input[name="newpassword2"]').val();
		if (nun.length < 1 || nun.length > 255) {
			layer.msg('请输入正确的用户名', {
				icon: 0,
				time: 1500
			}, function() {
				obj.find('input[name="newusername"]').focus();
			});
			return false;
		}
		if (opw.length < 1 || opw.length > 255) {
			layer.msg('请输入正确的原密码', {
				icon: 0,
				time: 1500
			}, function() {
				obj.find('input[name="oldpassword"]').focus();
			});
			return false;
		}
		if (npw.length < 1 || npw.length > 255) {
			layer.msg('请输入新密码', {
				icon: 0,
				time: 1500
			}, function() {
				obj.find('input[name="newpassword"]').focus();
			});
			return false;
		}
		if (npw != npw2) {
			layer.msg('两次输入的密码不一致', {
				icon: 0,
				time: 1500
			}, function() {
				obj.find('input[name="newpassword"]').focus();
			});
			return false;
		}
		strs = {
			"username": nun,
			"oldpassword": opw,
			"newpassword": npw
		};
	}
	$(This).addClass('layui-btn-disabled').html(
		'<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop">&#xe63d;</i>&nbsp;保存中...');
	$.post(root_path + 'index.php/index/admin?c=savepassword', strs, function(data) {
		$(This).removeClass('layui-btn-disabled').html('&nbsp;保存&nbsp;');
		if (data['state'] == 'ok') {
			if (type == 'xgmm') {
				layer.alert('密码修改成功', {
					icon: 1,
					title: "提示"
				}, function(index) {
					layer.closeAll();
				});
				return false;
			}
		} else {
			layer.msg(data['datas']['msg'], {
				icon: 2,
				time: 1500
			});
		}
	});
}
