# coding:utf-8
from app import app
from flask import  render_template,jsonify,request,json,session
from app.data_vars.vars import u_data



@app.route('/api/reg',methods = ["POST"])
def post_apireg():
    data = request.form.to_dict()    # 获取表单的数据
    input_code=data['captcha']
    s_code = session.get('code')  # 获取前端图形验证码的值
    # print("得到session: "+s_code)
    # print(type(s_code))
    # print("表单输入的验证码为: "+input_code)
    if input_code == s_code:
        return jsonify({"datas": {"msg": "注册成功", "code": s_code}, "state": "ok"})
    else:
        return jsonify({"datas": {"msg": "图形验证码不正确", "code": s_code}, "state": "fail"})


@app.route('/api/login',methods = ["POST"])
def post_login():
    data = request.form.to_dict()    # 获取表单的数据
    input_code=data['captcha']
    s_code = session.get('code')  # 获取前端图形验证码的值
    if input_code == s_code:
        return jsonify({"datas": {"msg": "登录成功", "code": s_code}, "state": "ok"})
    else:
        return jsonify({"datas": {"msg": "图形验证码不正确", "code": s_code}, "state": "fail"})
