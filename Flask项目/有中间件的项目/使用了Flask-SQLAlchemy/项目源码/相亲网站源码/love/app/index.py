# coding:utf-8
from app import app
from flask import  render_template,jsonify,request
from app.data_vars.vars import u_data

## 前台页面
@app.route('/',methods=['GET', 'POST'])
def index():
    nowdate = 2019
    if request.method == 'POST':
        data = request.form.to_dict()
        print(data)         # 前端传过来的数据 进行修改
        return "this is post for search"
    else:
        return render_template('index.html', u_data=u_data, nowdate=nowdate)



@app.route('/reg')
def reg():
    return render_template('reg.html')

@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/my')
def my():
    return render_template('my.html')


@app.route('/detail')
def detail():
    return render_template('detail.html')

@app.route('/sexman')
def sexman():
    return render_template('sexman.html')

@app.route('/sexwoman')
def sexwoman():
    return render_template('sexwoman.html')


@app.route('/api/purone')
def purone():
    data={"code":1010003,"msg":"token未填写"}
    return jsonify(data)
