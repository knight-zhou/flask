# coding:utf-8
## 初始化flask以及连接数据库
from flask import Flask
from settings import Config
from flask_sqlalchemy import SQLAlchemy
from flask_cors import *
import os

##
app = Flask(__name__)
CORS(app, supports_credentials=True)  # 设置跨域
app.config['SECRET_KEY']=os.urandom(24)  #设置为24位的字符,每次运行服务器都是不同的，所以服务器启动一次上次的session就清除

app.config.from_object(Config)
db=SQLAlchemy(app)


### 一些控制器函数加进来,不然控制器的代码无法生效
from app import index,imgcode,api