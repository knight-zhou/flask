# coding:utf-8
from app import app
from flask_script import Manager,Server

manager = Manager(app)
# 启动调试
app.config['DEBUG'] = True

##########
manager.add_command("runserver",Server(host="0.0.0.0",port=5000))
if __name__=='__main__':
    manager.run()