#coding:utf-8
import os

basedir = os.path.abspath(os.path.dirname(__file__))

is_exist_admin = False


DIALECT = 'mysql'
# DRIVER='mysqldb'
DRIVER='pymysql'

USERNAME='knight'
PASSWORD='knight'
HOST = '192.168.30.143'
PORT = '3306'
DATABASE = 'blog_git'

DB_URI="{}+{}://{}:{}@{}:{}/{}?charset=utf8".format(DIALECT,DRIVER,USERNAME,PASSWORD,HOST,PORT,DATABASE)


class Config:
    SECRET_KEY = 'a string'
    # 数据库配置
    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass
