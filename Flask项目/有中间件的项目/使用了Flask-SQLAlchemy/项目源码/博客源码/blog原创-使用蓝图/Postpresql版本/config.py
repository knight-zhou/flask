#coding:utf-8
import os

basedir = os.path.abspath(os.path.dirname(__file__))

is_exist_admin = False


# DIALECT = 'pgql'
DRIVER='postgresql'

USERNAME='postgres'
PASSWORD='postgres'
HOST = 'localhost'
PORT = '5432'
DATABASE = 'blog'

DB_URI="{}://{}:{}@{}:{}/{}".format(DRIVER,USERNAME,PASSWORD,HOST,PORT,DATABASE)


class Config:
    SECRET_KEY = 'a string'
    # 数据库配置
    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass
