## 环境依赖:
```
pip3 install flask
pip3 install flask-script
pip3 install flask-bootstrap
pip3 install flask-wtf
pip3 install flask-sqlalchemy
pip3 install flask-login
```

## 创建数据库的表结构
```
python manage.py db init   # 在创建 question表的时候这条省略
python manage.py db migrate   创建迁移
python manage.py db upgrade
```


## 注意:
文章标题一致就是更新