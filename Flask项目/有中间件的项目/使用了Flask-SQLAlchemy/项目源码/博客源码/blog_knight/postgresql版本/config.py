#!/usr/bin/env python
#coding:utf-8

class Config(object):
    """Base config class."""
    pass

class ProdConfig(Config):
    """Production config class."""
    pass

class DevConfig(Config):
    """Development config class."""
    DEBUG = True
    # 去掉警告
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # python2使用postgresql
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost:5432/blog'




