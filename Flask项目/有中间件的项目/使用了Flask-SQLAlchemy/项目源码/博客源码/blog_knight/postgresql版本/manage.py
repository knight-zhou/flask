#!/usr/bin/env python
#coding:utf-8

# import Flask Script object
from flask_script import Manager,Server
from flask_migrate import Migrate,MigrateCommand
from main import app
from models import User,db

manager = Manager(app)

#使用migrate绑定app和db
migrate = Migrate(app,db)

#添加迁移脚本的命令到manger中,MigrateCommand会读取导入的模型
manager.add_command('db',MigrateCommand)


if __name__ == '__main__':
    manager.run()



