#!/usr/bin/env python
#coding:utf-8

class Config(object):
    """Base config class."""
    pass

class ProdConfig(Config):
    """Production config class."""
    pass

class DevConfig(Config):
    """Development config class."""
    DEBUG = True
    # 去掉警告
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # python2使用mysqldb
    SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://knight:knight@192.168.30.143:3306/blog_knight'




