#!/usr/bin/env python
#coding:utf-8

from flask import Flask
from config import DevConfig

app = Flask(__name__)

app.config.from_object(DevConfig)


# 指定 URL='/' 的路由规则
# 当访问 HTTP://server_ip/ GET(Default) 时，call home()
@app.route('/')
def home():
    return '<h1>Hello World! I am Knight</h1>'

if __name__ == '__main__':
    # Entry the application
    app.run()


