## 生成 requirements.txt
pip freeze > requirements.txt

## 模仿Django的shell
python manage.py shell

## 创建表结构

```
python manage.py db init   # 在创建 question表的时候这条省略
python manage.py db migrate   创建迁移
python manage.py db upgrade
```

## 插入数据
```
python manage.py shell
>>>from uuid import uuid4
>>>from models import User
>>>from models import db
>>>user = User(id=str(uuid4()), username='knight', password='123456')
>>>db.session.add(user)
>>>db.session.commit()
```

[flask的SQLAlchemy的ORM举例](http://blog.csdn.net/jmilk/article/details/53187575/)


## models.py 详解
```
(1)db.relationsformat(self.username)hip： 会在 SQLAlchemy 中创建一个虚拟的列，该列会与 Post.user_id (db.ForeignKey) 建立联系。这一切都交由 SQLAlchemy 自身管理。

(2)backref：用于指定表之间的双向关系，如果在一对多的关系中建立双向的关系，这样的话在对方看来这就是一个多对一的关系。

(3)lazy：指定 SQLAlchemy 加载关联对象的方式。

    lazy=subquery: 会在加载 Post 对象后，将与 Post 相关联的对象全部加载，这样就可以减少 Query 的动作，也就是减少了对 DB 的 I/O 操作。

    但可能会返回大量不被使用的数据，会影响效率。
    lazy=dynamic: 只有被使用时，对象才会被加载，并且返回式会进行过滤，如果现在或将来需要返回的数据量很大，建议使用这种方式。Post 就属于这种对象

```

