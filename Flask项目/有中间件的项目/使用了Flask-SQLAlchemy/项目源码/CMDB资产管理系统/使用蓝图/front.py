#!/usr/bin/env python
#coding:utf-8
from  flask import Blueprint,render_template,redirect
from models import Host

front = Blueprint('front',__name__)

@front.route('/')
def index():
    posts = Host.query.all()
    return render_template('cmdb_index.html',posts= posts)


@front.route('/help/')
def help():
    return u'这是help页面...'