#!/usr/bin/env python
#coding:utf-8


from flask import Flask,render_template,request,redirect,url_for,session,g
import config
from exts import db
from decorators import login_required
from sqlalchemy import or_
from admin import admin
from front import front



app = Flask(__name__)
app.config.from_object(config)

##切记不要忘记了这个初始化
db.init_app(app)

# 注册蓝图
app.register_blueprint(admin,url_prefix='/admin')
app.register_blueprint(front,url_prefix='/front')


if __name__=='__main__':
    app.run()
