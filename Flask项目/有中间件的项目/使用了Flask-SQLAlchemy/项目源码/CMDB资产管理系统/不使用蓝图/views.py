#!/usr/bin/env python
#coding:utf-8


from flask import Flask,render_template,request,redirect,url_for,session,g
import config
from exts import db
from decorators import login_required
from sqlalchemy import or_
from models import Host




app = Flask(__name__)
app.config.from_object(config)

##切记不要忘记了这个初始化
db.init_app(app)


@app.route('/')
def index():
    posts = Host.query.all()
    return render_template('cmdb_index.html',posts= posts)



if __name__=='__main__':
    app.run()
