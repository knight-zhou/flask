#!/usr/bin/env python
#coding:utf-8

from exts import db
from datetime import datetime
from werkzeug.security import generate_password_hash,check_password_hash

class Host(db.Model):
    __tablename__ = 'host'
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    hostname = db.Column(db.String(50),nullable=False)
    ip = db.Column(db.String(50),nullable=False)
    osversion = db.Column(db.String(50), nullable=False)
    memory = db.Column(db.String(50), nullable=False)
    disk = db.Column(db.String(50), nullable=False)
    vendor_id = db.Column(db.String(50))
    model_name = db.Column(db.String(50))
    cpu_core = db.Column(db.String(50))
    product = db.Column(db.String(50))
    Manufacturer  = db.Column(db.String(50))
    Sn = db.Column(db.String(50))

  