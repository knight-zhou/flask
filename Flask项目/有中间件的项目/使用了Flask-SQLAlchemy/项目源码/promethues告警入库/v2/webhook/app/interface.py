# coding:utf-8
from app import app
import json
from flask import request
from common.insert_data import insert as charu

# charu.insert(data)
@app.route('/ruku', methods=['POST'])
def send():
    data = json.loads(request.data)   # 这是字典
    charu.insert(data)
    return "ok"

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug="true",port=5001)
