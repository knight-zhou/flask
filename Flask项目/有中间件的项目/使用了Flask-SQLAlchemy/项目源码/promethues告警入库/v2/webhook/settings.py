# coding:utf-8
class Config(object):
    DEVICE = 'mysql'
    DRIVER  = 'pymysql'
    USERNAME = 'knight'
    PASSWORD = 'zhoulong'
    HOST = 'yy.cn'
    # HOST = '172.19.192.86'
    PORT = '3306'
    DATABASE = 'proms_ruku'

    SQLALCHEMY_DATABASE_URI = '{}+{}://{}:{}@{}:{}/{}?charset=utf8'.format(
        DEVICE, DRIVER, USERNAME, PASSWORD, HOST, PORT, DATABASE
    )
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True