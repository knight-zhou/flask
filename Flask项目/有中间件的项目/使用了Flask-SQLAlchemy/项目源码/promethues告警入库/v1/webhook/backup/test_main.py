from flask import Flask, request, abort
import json
app = Flask(__name__)

@app.route('/ruku', methods=['POST'])
def webhook():
    if request.method == 'POST':
        data = request.get_data()    #dict
        dict_data = json.loads(data.decode("utf-8"))
        print(type(dict_data))
        return "ok"
    else:
        abort(400)

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug="true",port=5001)