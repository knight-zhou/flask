# coding:utf-8
from flask_sqlalchemy import SQLAlchemy
from flask import Flask,request
from settings import Config
import json
from tools import insert_data as charu

app = Flask(__name__)
app.config.from_object(Config)


db=SQLAlchemy(app)

# charu.insert(data)
@app.route('/ruku', methods=['POST'])
def send():
    data = json.loads(request.data)   # 这是字典
    charu.insert(data)
    return "ok"

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug="true",port=5001)
