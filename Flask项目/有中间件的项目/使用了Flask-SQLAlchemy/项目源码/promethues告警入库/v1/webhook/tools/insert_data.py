# coding:utf-8
from models import Proms_alert,Proms_annotations,Proms_labels
from manage import db
from tools.timechange import utc2local
# data={'receiver': 'devops\\.wechatmail', 'status': 'resolved', 'alerts': [{'status': 'resolved', 'labels': {'alertname': 'tcp_port_check failed', 'app': 'tms-admin', 'group': 'tms', 'hostname': 'hwy-hn1-transport-admin-prd-0', 'instance': '172.19.192.34:80833', 'job': 'tcp_port_check', 'serverity': 'critical'}, 'annotations': {'description': 'tms的tms-admin tcp检测失败,当前probe_success的值为0', 'summary': 'tms组的应用 tms-admin 端口检测不通'}, 'startsAt': '2019-10-29T20:45:06.631494166+08:00', 'endsAt': '2019-10-29T20:48:21.631494166+08:00', 'generatorURL': 'http://hwy-hn1-carsales-baseservice:9090/graph?g0.expr=probe_success%7Bjob%3D%22tcp_port_check%22%7D+%3D%3D+0&g0.tab=1'}], 'groupLabels': {'alertname': 'tcp_port_check failed'}, 'commonLabels': {'alertname': 'tcp_port_check failed', 'app': 'tms-admin', 'group': 'tms', 'hostname': 'hwy-hn1-transport-admin-prd-0', 'instance': '172.19.192.34:80833', 'job': 'tcp_port_check', 'serverity': 'critical'}, 'commonAnnotations': {'description': 'tms的tms-admin tcp检测失败,当前probe_success的值为0', 'summary': 'tms组的应用 tms-admin 端口检测不通'}, 'externalURL': 'http://hwy-hn1-carsales-baseservice:9093', 'version': '4', 'groupKey': '{}:{alertname="tcp_port_check failed"}'}

def insert(data):
    aa = data['alerts']
    bb = aa[0]
    ## 先插入第一张表
    proms_laert = Proms_alert(bb['status'], utc2local(bb['startsAt']), utc2local(bb['endsAt']))
    print("eeeeeeee"+bb['endsAt'])
    ## 查询最后一条记录找到id号
    proms_alert_chaxun = Proms_alert.query.order_by(Proms_alert.id.desc()).first()
    proms_alert_id=proms_alert_chaxun.id+1    # 查询记录得到值,得到关联的id值
    ## 插入标签表
    cc = bb['labels']
    # proms_labels = Proms_labels(app=cc['app'],group=cc['group'],serverity=cc['serverity'],instance=cc['instance'],hostname=cc['hostname'],proms_alert_id=proms_alert_id)
    proms_labels = Proms_labels(cc['app'],cc['group'],cc['serverity'],cc['instance'],cc['hostname'],proms_alert_id=proms_alert_id)
    ## 插入总结表
    dd = bb['annotations']
    proms_annotations=Proms_annotations(dd['description'],dd['summary'],proms_alert_id)

    ## 提交更新数据
    db.session.add(proms_laert)
    db.session.add(proms_labels)
    db.session.add(proms_annotations)
    db.session.commit()
