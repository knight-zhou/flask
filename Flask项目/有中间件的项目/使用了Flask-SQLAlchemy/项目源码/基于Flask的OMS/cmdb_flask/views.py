#!/usr/bin/env python
#coding:utf-8


from flask import Flask,render_template,request,redirect,url_for,session,g
import config
from exts import db
from decorators import login_required
from sqlalchemy import or_
from admin import admin
from front import front
from user import user



app = Flask(__name__)
app.config.from_object(config)

##切记不要忘记了这个初始化
db.init_app(app)

# 注册蓝图
app.register_blueprint(admin,url_prefix='/admin')
app.register_blueprint(front,url_prefix='/front')
app.register_blueprint(user,url_prefix='/user')

# oms前台首页
@app.route('/')
def index():
    return u'这是oms前台首页内容...'



if __name__=='__main__':
    app.run()
