#!/usr/bin/env python
#coding:utf-8

from exts import db
from datetime import datetime
from werkzeug.security import generate_password_hash,check_password_hash

class HostList(db.Model):
    __tablename__ = 'hostList'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ip = db.Column(db.String(50),nullable=False)
    hostname = db.Column(db.String(50),nullable=False)
    product = db.Column(db.String(50))
    application = db.Column(db.String(50))
    idc_jg = db.Column(db.String(50))
    status = db.Column(db.String(50))
    remark = db.Column(db.String(50))
    product = db.Column(db.String(50))
    host_group = db.Column(db.String(50))



class ServerAsset(db.Model):
    __tablename__ = 'serverAsset'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    manufacturer = db.Column(db.String(20),doc=u'厂商')
    productname = db.Column(db.String(30),doc =u'产品型号')
    cpu_model = db.Column(db.String(30),doc=u'CPU型号')
    service_tag = db.Column(db.String(30),doc=u'序列号')
    ip = db.Column(db.String(30),doc=u'ip地址')
    os = db.Column(db.String(30),doc=u'操作系统')
    virtual = db.Column(db.String(30),doc=u'是否为虚拟机')
    idc_name = db.Column(db.String(30),doc=u'所属机房')
    hostname = db.Column(db.String(30),doc=u'主机名')


class NetworkAsset(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ip = db.Column(db.String(20), doc=u'ip地址')
    hostname = db.Column(db.String(30), doc=u'主机名')
    remark = db.Column(db.String(30), doc=u'备注',default='')


class IdcAsset(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idc_name = db.Column(db.String(20), doc=u'机房名称')
    idc_type = db.Column(db.String(20), doc=u'机房类型')
    idc_location = db.Column(db.String(20), doc=u'机房位置')
    contract_date = db.Column(db.String(20), doc=u'合同时间')
    idc_contacts = db.Column(db.String(20), doc=u'联系电话')
    remark = db.Column(db.String(20), doc=u'备注',default='')














