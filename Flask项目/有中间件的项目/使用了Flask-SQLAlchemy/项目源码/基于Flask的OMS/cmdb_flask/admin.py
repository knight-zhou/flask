#!/usr/bin/env python
#coding:utf-8
from  flask import Blueprint,render_template,redirect
from models import HostList,IdcAsset

# 实例化
admin = Blueprint('admin',__name__)


#定义后台首页
@admin.route('/')
def index():
    return render_template('hindex.html')


@admin.route('/list/')
def host_list():
    alltlist = HostList.query.all()
    context = {
        'list': HostList.query.all()

    }

    return render_template('host_list.html',**context)

@admin.route('/center/')
def data_center():
    posts = IdcAsset.query.all()
    context = {
        'list': posts

    }
    return render_template('center.html',**context)


