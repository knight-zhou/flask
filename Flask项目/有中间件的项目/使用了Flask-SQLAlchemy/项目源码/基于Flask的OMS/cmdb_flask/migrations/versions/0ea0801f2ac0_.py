"""empty message

Revision ID: 0ea0801f2ac0
Revises: 
Create Date: 2017-08-24 18:09:36.330000

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0ea0801f2ac0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('hostList',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('ip', sa.String(length=50), nullable=False),
    sa.Column('hostname', sa.String(length=50), nullable=False),
    sa.Column('application', sa.String(length=50), nullable=True),
    sa.Column('idc_jg', sa.String(length=50), nullable=True),
    sa.Column('status', sa.String(length=50), nullable=True),
    sa.Column('remark', sa.String(length=50), nullable=True),
    sa.Column('product', sa.String(length=50), nullable=True),
    sa.Column('host_group', sa.String(length=50), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('idc_asset',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('idc_name', sa.String(length=20), nullable=True),
    sa.Column('idc_type', sa.String(length=20), nullable=True),
    sa.Column('idc_location', sa.String(length=20), nullable=True),
    sa.Column('contract_date', sa.String(length=20), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('network_asset',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('ip', sa.String(length=20), nullable=True),
    sa.Column('hostname', sa.String(length=30), nullable=True),
    sa.Column('remark', sa.String(length=30), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('serverAsset',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('manufacturer', sa.String(length=20), nullable=True),
    sa.Column('productname', sa.String(length=30), nullable=True),
    sa.Column('cpu_model', sa.String(length=30), nullable=True),
    sa.Column('service_tag', sa.String(length=30), nullable=True),
    sa.Column('ip', sa.String(length=30), nullable=True),
    sa.Column('os', sa.String(length=30), nullable=True),
    sa.Column('virtual', sa.String(length=30), nullable=True),
    sa.Column('idc_name', sa.String(length=30), nullable=True),
    sa.Column('hostname', sa.String(length=30), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('serverAsset')
    op.drop_table('network_asset')
    op.drop_table('idc_asset')
    op.drop_table('hostList')
    # ### end Alembic commands ###
