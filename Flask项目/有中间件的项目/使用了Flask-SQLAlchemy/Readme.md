##### Flask官方文档

中文文档：  https://dormousehole.readthedocs.io/en/latest/



##### Flask-SQLAlchemy 官方文档

 官方文档： https://flask-sqlalchemy.palletsprojects.com/en/2.x



 中文官方文档（内容已过时）： http://www.pythondoc.com/flask-sqlalchemy/

```
# 下面的这个包已经没有
from flask.ext.sqlalchemy import SQLAlchemy
```



 连接参数： https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/

 



