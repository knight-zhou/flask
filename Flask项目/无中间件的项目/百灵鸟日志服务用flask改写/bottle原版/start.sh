#!/bin/bash

gunicorn="/usr/local/python2.7.11/bin/gunicorn"

nohup ${gunicorn} --workers=4 logstore:application -b 0.0.0.0:9898 > /dev/null 2>&1 &
