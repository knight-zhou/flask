#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.config
from bottle import run,route,request,response,abort,error,get,post,put,default_app,Bottle
import json
import time

_DEFAULT_LOG_CONFIG = './logconfig.ini'

logging.config.fileConfig(_DEFAULT_LOG_CONFIG, disable_existing_loggers=False)
logger = logging.getLogger(__name__)

@post('/logstore/upload')
def update():
	try:
		logger.info(request.body.getvalue())
	except Exception as e:
		pass
	logid = "log_" + str(int(time.time()))
	response = {
		"errcode":200,
		"errmsg":"OK",
		"version":"1.0",
		"logid":logid
	}
	return json.dumps(response)


application = default_app()
