#!/usr/bin/env python
#coding:utf-8
import  logging
import logging.config
import json,time

from flask import Flask,request

_DEFAULT_LOG_CONFIG = './logconfig.ini'
logging.config.fileConfig(_DEFAULT_LOG_CONFIG, disable_existing_loggers=False)
logger = logging.getLogger(__name__)

app = Flask(__name__)


@app.route('/logstore/upload/',methods=['GET','POST'])
def update():
    try:
        logger.info(request.data)
    except Exception as e:
        pass
    logid = "log_" + str(int(time.time()))
    response = {
        "errcode": 200,
        "errmsg": "OK",
        "version": "1.0",
        "logid": logid
    }
    return json.dumps(response)

if __name__ == '__main__':
    app.run()