#### 上下文管理器
```
上下文就是一个变量传递的过程，是个概念性的东西，不是函数 不是方法.
所以无需纠结,比如 g 对象取上下文的值, request.get['xx'] 取前台传过来的变量 等等.

```

#### rrdtool 绘图
```
rrdtool（round robin database）工具为环状数据库的存储格式，round robin是一种处理定量数据以及当前元素指针的技术

rrdtool主要用来跟踪对象的变化情况，生成这些变化的走势图，比如业务的访问流量、系统性能、磁盘利用率等趋势图

很多流行监控平台都使用到rrdtool，比较有名的为Cacti、Ganglia、Monitorix等
```

#### flask 权限管理
```
网上很多源码都采用flask-admin进行控制的
```

#### 为什么要用flask_restful库去实现rest接口
好处: 额外提供了put,delete等方法

使用rest库
```
# coding:utf-8
from flask import Flask
from flask_restful import  Resource,Api

app=Flask(__name__)
api=Api(app)

class Student(Resource):
    def get(self):
        return {'name':"tom","age":18}

## 路由
api.add_resource(Student,'/')

if __name__ == '__main__':
    app.run(debug=True)
```

不使用rest库:
```
# coding:utf-8
from flask import Flask,jsonify
app = Flask(__name__)
data={"name":"tom","city":"cs"}

@app.route('/task',methods=['GET', 'POST'])
def hello_world():
    return jsonify(data)

# curl 127.0.0.1:5000/v1/aa
@app.route('/v1/<name>')
def canshu(name=None):
    return jsonify(name)

if __name__ == '__main__':
    app.run(debug=True)

```

#### 使用`gunicorn`启动应用
main.py 代码如下
```python
# coding:utf-8
from flask import  Flask
app = Flask(__name__)

@app.route('/')
def index():
    return "hello world"

@app.route('/demo')
def demo():
    return "this is demo!"

if __name__ == '__main__':
    app.run()
```
启动方式如下:
```shell
# main 是main.py的名字,app是flask应用的名字
/usr/local/python36/bin/gunicorn -w 3 -b :5000 -t 600 main:app    

# -w 是指定的运行线程  

```