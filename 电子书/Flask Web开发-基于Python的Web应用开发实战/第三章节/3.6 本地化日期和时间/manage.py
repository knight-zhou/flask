#coding:utf-8
from flask import Flask, render_template
from datetime import datetime


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html',c_time=datetime.utcnow())


@app.route('/user/<name>')
def user(name):
    return render_template('user.html', name=name)


if __name__ == '__main__':
    app.run('0.0.0.0')



