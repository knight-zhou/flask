### Nginx结合Flask项目注意事项

#### 背景

前端在解决跨域的问题时候，我要前端写了一个Demo，但是了我们在nginx进行转发的时候，一直提示404，但是了直接通过IP和端口请求是正常的。

起初的nginx的配置如下：

```bash
upstream xxoo_api {
        server localhost:5000 max_fails=3 fail_timeout=3s weight=1;
}

server
{
       listen       80;
       server_name  test3.yeyese.top; 

       #location /xxoo {
       location /xxoo {
                  proxy_pass http://xxoo_api;
           }

        access_log /home/data/logs/blog/test.access.log access;
}
```

flask的项目配置如下： 

```python
from flask import Flask, jsonify, request
import logging

import json
app = Flask(__name__)

data = {
        "code":0,
        "data":{
                "userId":564
        },
        "msg":"success"
}


@app.route("/get")
def hi():
    return "this is get request"


@app.route("/user",methods=['POST'])
def getUserInfo():
    #post_data = request.form['source']
    return jsonify(data)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
```

很奇怪的是，当我们的nginx的配置如果直接匹配根，然后请求`test3.yeyese.top/user` 是有数据返回的。

```bash
       location / {
                  proxy_pass http://xxoo_api;
           }
```

但是加了`/xxoo`，就不行，并且flask的标准输出有提示已经请求进来了

```
127.0.0.1 - - [02/Sep/2020 14:42:12] "POST //user HTTP/1.0" 404 -
```

#### 原因

当时我一直以为，只要nginx加了配置，然后就可以跳转到Python应用（我以为也和Java一样）。后面我发现不是这样的。Java之所以可以这样，是因为实现和运维沟通了匹配的路由规则，Java开发已经在他的项目里已经配置了相同的路由了。所以你才会认为是只要加了Nginx的配置规则就会转发过去。并且Java项目不需要匹配对应的路由。

#### 解决

要解决这个问题，我们的Nginx的配置文件`/xxoo`不需要改变，只需要更改flask项目的路由就可以了。

```
from flask import Flask, jsonify, request
import logging

import json
app = Flask(__name__)

data = {
        "code":0,
        "data":{
                "userId":564
        },
        "msg":"success"
}


@app.route("/get")
def hi():
    return "this is get request"


@app.route("/xxoo/user",methods=['POST'])
def getUserInfo():
    #post_data = request.form['source']
    return jsonify(data)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
```

这样子再去使用 postman或者使用Python进行请求就可以正常的数据返回了。请求代码如下:

```
import requests
url = "http://test3.yeyese.top/xxoo/user"

params = {"clueFromCode":"","phone":18877210282,"source":"H5"}

r = requests.post(url=url,data=params)
print(r.text)

```

返回：

```
{
  "code": 0, 
  "data": {
    "userId": 564
  }, 
  "msg": "success"
}
```

